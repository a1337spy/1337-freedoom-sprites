# 1337-freedoom-sprites

This mod adds various edits to some sprites and sounds in Freedoom, based on my own personal preferences. This is intended to be used with Hideous Destructor.

## Changes
	Changed pistol sprites to the beretta sprites made by Tesefy/Sonik.O.Fan (the drop sprite is unchanged, however)
	Added greebling on both sides of the cell battery to make it more visually interesting, along with making the charge indicator bigger
	Adjusted offsets for the hunter pumping frames to make them move a bit more visually satisfying
	Adjusted SMG drop sprite to be less slanted
	Replaced Large Technospider sprites with the Director sprites from Hell Forged
	Replaced Lightamp sprites with sprites by Tesefy/Sonik.O.Fan to match Lightamp overlay
	Replaced Pain Lord sight and death sounds with Archon of Hell sight/death sounds by Amiscura

## Credits
	Sonik.O.Fan/Tesefy - Beretta sprites, Lightamp sprites
	Amiscura - Director sprites, Archon of Hell sight/death sounds